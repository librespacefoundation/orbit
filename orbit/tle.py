import math

from lxml import html
import requests
import ephem


def get(catnr):
    page = html.fromstring(requests.get('https://www.celestrak.com/satcat/tle.php?CATNR=%s' % catnr).text)
    tle = page.xpath('//pre/text()')[0].split('\n')
    return tle[1].strip(), tle[2].strip(), tle[3].strip()
    
def parse(name, line1, line2):
    tle_rec = ephem.readtle(name, line1, line2)
    tle_rec.compute()
    return tle_rec
